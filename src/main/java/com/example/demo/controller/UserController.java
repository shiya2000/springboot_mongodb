package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.service.UserServcie;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * @ClassName UserController
 * @Description TODO
 * @Author shiya
 * @Date 2019/5/31 9:48
 * @Version 1.0
 */
@Api(description = "用户Api")
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserServcie userServcie;

    @ApiOperation("查询所有用户列表")
    @GetMapping
    public List<User> findAll(@ApiParam(value = "页码",name = "page",defaultValue = "1")@RequestParam("page")Integer page,
                              @ApiParam(value = "记录数",name = "size",defaultValue = "10")@RequestParam("size")Integer size){
        return userServcie.list(page,size);
    }

    @ApiOperation("通过id查询")
    @GetMapping("/{id}")
    public User findOneById(@PathVariable Long id){
        return userServcie.getOneById(id);
    }
    @ApiOperation("通过名称模糊查询")
    @GetMapping("/like/{name}")
    public List<User> findOneByName(@PathVariable String name){
        return userServcie.getListLikeName(name);
    }
    @ApiOperation("添加")
    @PostMapping
    public User save(@RequestBody User user){
        return userServcie.save(user);
    }
    @ApiOperation("修改")
    @PutMapping
    public String update(@RequestBody User user){
        return userServcie.update(user);
    }
    @ApiOperation("删除")
    @DeleteMapping
    public Long del(@RequestBody User user){
        return userServcie.del(user);
    }
}
