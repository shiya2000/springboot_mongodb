package com.example.demo.model;
import lombok.Data;
import org.springframework.data.annotation.Id;
/**
 * @ClassName User
 * @Description TODO 用户实体
 * @Author shiya
 * @Date 2019/5/30 17:23
 * @Version 1.0
 */
@Data
public class User {
    /**
     * 用户id
     **/
    @Id
    private Long id;
    /**
     * 用户名
     **/
    private String username;
    /**
     * 用户年龄
     **/
    private Integer age;
}
