package com.example.demo.service;

import com.example.demo.model.User;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

/**
 * @ClassName UserServcie
 * @Description TODO 业务
 * @Author shiya
 * @Date 2019/5/30 17:31
 * @Version 1.0
 */
@Service
@Slf4j
public class UserServcie {
    @Autowired
    private MongoTemplate mongoTemplate;
    /**
     * @Author shiya
     * @Description TODO 添加用户
     * @Date 17:37 2019/5/30
     * @Param [user]
     * @return com.example.demo.model.User
     **/
    public User save (User user){
        log.info("添加用户：" + user);
        User save = mongoTemplate.save(user);
        log.info("添加成功用户：" + save);
        return save;
    }
    /**
     * @Author shiya
     * @Description TODO 用户列表
     * @Date 17:39 2019/5/30
     * @Param []
     * @return java.util.List<com.example.demo.model.User>
     **/
    public List<User> list(Integer page,Integer size){
        Query query = new Query();
        //分页
        query.skip((page - 1)/size);
        query.limit(size);
        //排序
        query.with(new Sort(new Sort.Order(Sort.Direction.ASC,"age")));
        List<User> all = mongoTemplate.find(query,User.class);
        log.info("用户列表：" + all);
        return all;
    }
    /**
     * @Author shiya
     * @Description TODO 通过id查询
     * @Date 17:47 2019/5/30
     * @Param [id]
     * @return com.example.demo.model.User
     **/
    public User getOneById(Long id){
        Query query = new Query(Criteria.where("_id").is(id));
        User one = mongoTemplate.findOne(query, User.class);
        log.info("用户：" + one);
        return one;
    }
    /**
     * @Author shiya
     * @Description TODO 根据用户名模糊查询
     * @Date 18:00 2019/5/30
     * @Param [name]
     * @return java.util.List<com.example.demo.model.User>
     **/
    public List<User> getListLikeName(String name){
        //完全匹配
//        Pattern pattern = Pattern.compile("^张$", Pattern.CASE_INSENSITIVE);
        //右匹配
//        Pattern pattern = Pattern.compile("^.*张$", Pattern.CASE_INSENSITIVE);
        //左匹配
//        Pattern pattern = Pattern.compile("^张.*$", Pattern.CASE_INSENSITIVE);
        //模糊匹配
        Pattern pattern= Pattern.compile("^.*"+name+".*$", Pattern.CASE_INSENSITIVE);
        Query query = new Query(Criteria.where("username").regex(pattern));
        List<User> users = mongoTemplate.find(query, User.class);
        log.info("模糊搜索的用户：" + users);
        return users;
    }
    /**
     * @Author shiya
     * @Description TODO 修改用户
     * @Date 9:14 2019/5/31
     * @Param [user]
     * @return java.lang.String
     **/
    public String update(User user){
        Query query = new Query(Criteria.where("_id").is(user.getId()));
        Update update = Update.update("username", user.getUsername()).set("age", user.getAge());
        //修改第一个符合条件的数据
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, User.class);
        //修改全部符合条件的，当set的key不存在会新建一个key,inc方法用于做累加操作,rename方法用于修改key的名称,unset方法用于删除key
//        mongoTemplate.updateMulti(query,update,User.class);
        //当没有符合条件的文档，就以这个条件和更新文档为基础创建一个新的文档，如果找到匹配的文档就正常的更新
//        mongoTemplate.upsert(query,update,User.class);
        log.info("=========> " + updateResult.getModifiedCount());
        return "success";
    }
    /**
     * @Author shiya
     * @Description TODO 删除
     * @Date 9:47 2019/5/31
     * @Param [user]
     * @return java.lang.Long
     **/
    public Long del(User user){
        DeleteResult remove = mongoTemplate.remove(user);
        log.info("删除影响的数量:" + remove.getDeletedCount());
        return remove.getDeletedCount();
    }
}
